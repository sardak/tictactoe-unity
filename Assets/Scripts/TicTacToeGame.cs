﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class TicTacToeGame : MonoBehaviour
{
    private enum BoardSpaceState
    {
        None,
        Circle,
        X,
        Tie,
    }

    [SerializeField]
    private List<Button> buttons;

    [SerializeField]
    private GameObject[] results;

    [SerializeField]
    private Sprite[] sprites;

    [SerializeField]
    private string[] lines;

    [SerializeField]
    private Image turnIndicator;

    private BoardSpaceState[] state;
    private int turn;

    public void Start()
    {
        state = new BoardSpaceState[buttons.Count];
        buttons.ForEach(button => button.onClick.AddListener(() => MakeMove(button)));
    }

    private void MakeMove(Button button)
    {
        var index = buttons.IndexOf(button);

        button.GetComponent<Image>().sprite = sprites[turn];
        button.interactable = false;

        state[index] = (BoardSpaceState)(turn + 1);
        turn = 1 - turn;
        turnIndicator.sprite = sprites[turn];

        if (!CheckForWinner() && !CanContinue())
        {
            ShowResults(BoardSpaceState.Tie);
        }
    }

    private bool CheckForWinner()
    {
        var result = GetValidLine((values) =>
        {
            if (!values.Contains(BoardSpaceState.None) && values.TrueForAll(value => value == values[0]))
            {
                return values[0];
            }

            return BoardSpaceState.None;
        });

        ShowResults(result);
        return result != BoardSpaceState.None;
    }

    private bool CanContinue()
    {
        return BoardSpaceState.None != GetValidLine((values) =>
        {
            var sum = System.Convert.ToInt32(values.Contains(BoardSpaceState.Circle));
            sum += System.Convert.ToInt32(values.Contains(BoardSpaceState.X));

            return (BoardSpaceState)(2 - sum);
        });
    }

    private BoardSpaceState GetValidLine(System.Func<List<BoardSpaceState>, BoardSpaceState> lineAnalyzer)
    {
        var lineValues = new List<BoardSpaceState>();
        BoardSpaceState result = BoardSpaceState.None;

        for (int index = 0, count = lines.Length; (index < count) && (result == BoardSpaceState.None); index++)
        {
            lineValues.Clear();
            lineValues.AddRange(lines[index].Select(c => state[c - '0']));

            result = lineAnalyzer.Invoke(lineValues);
        }

        return result;
    }

    private void ShowResults(BoardSpaceState result)
    {
        if (result != BoardSpaceState.None)
        {
            results[(int)result - 1].SetActive(true);
        }
    }
}
